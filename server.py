import datetime
import os
import socket
import sys
import time
import _thread

import argparse
import psycopg2
from crontab import CronTab

from shared import RequestType, Protocol


HOST = 'localhost'
PORT = int(os.getenv('SERVER_PORT'))


def execute_file(filename):
    with open(filename) as f:
        cur = conn.cursor()
        cur.execute(f.read())


def set_up_scheduled_trains(delay_days):
    cursor = conn.cursor()
    cursor.execute("SELECT id, cron_schedule from train;")
    now = datetime.datetime.now()

    for (id, cron_schedule) in cursor.fetchall():
        try:
            cron = CronTab(cron_schedule)
        except Exception as ex:
            print(f"invalid cron for train id {id}")
            continue

        for delay in delay_days:
            dt = cron.next(now=now + datetime.timedelta(days=delay), return_datetime=True)
            cursor.execute(f"""CALL schedule_train('{id}', '{dt}');""")

    print(f"Debug: added scheduled trains")


def continuous_train_addition():
    while True:
        time.sleep(60 * 60 * 24)
        set_up_scheduled_trains(range(21, 20, -1))


def continuous_train_deletion():
    while True:
        time.sleep(60 * 60)
        conn.cursor().execute("UPDATE scheduled_train SET done = TRUE WHERE CURRENT_TIMESTAMP > start_dt;")


class Session:
    # escape sql injections
    def __init__(self, cursor):
        self.user_id = None
        self.cursor = cursor

    def handle_request(self, req):
        ####################################################
        if req['type'] == RequestType.AUTHORIZE:
            self.cursor.execute(f'''SELECT authorize(%s, %s);''', (req['data']['username'], req['data']['password']))
            answer = {'code': self.cursor.fetchone()[0]}

            if answer['code'] == 0:
                self.cursor.execute('''SELECT "user".id FROM "user" WHERE "user".username = %s; ''', (req['data']['username'],))
                self.user_id = self.cursor.fetchone()[0]
            return answer
        elif req['type'] == RequestType.LOGOUT:
            self.user_id = None
            return {'code': 0}
        elif req['type'] == RequestType.REGISTER:
            self.cursor.execute(f'''SELECT register_user(%s, %s);''', (req['data']['username'], req['data']['password']))
            return {'code': self.cursor.fetchone()[0]}

        #####################################################
        elif req['type'] == RequestType.FIND_TRAIN:
            try:
                datetime.datetime.strptime(req['data']['where'], '%Y-%m-%d')
            except ValueError:
                return {'code': 1, 'msg': 'Неверный формат даты'}
            from_st = req['data']['from_st']
            where = req['data']['where']
            to_st = req['data']['to_st']

            self.cursor.execute(f"""SELECT sc_tr.id, sc_tr.train_id, sc_tr.start_dt::varchar from scheduled_train sc_tr 
                                    INNER JOIN train_station tr_st ON (sc_tr.train_id = tr_st.train_id) 
                                    INNER JOIN station st ON (tr_st.station_id = st.id) 
                                    WHERE sc_tr.start_dt::date = %s AND done = FALSE AND (st.name = %s OR st.name = %s)
                                    GROUP BY (sc_tr.id, sc_tr.train_id)
                                    HAVING string_agg(st.name, '|') LIKE %s;""", (where, from_st, to_st, f"%{from_st}%{to_st}%"))
            trains = self.cursor.fetchall()
            out = {}
            for train in trains:
                self.cursor.execute(f"""SELECT tr.code, st.name, tr_st."order", tr_st.elapsed_minutes, tr_st.multiplier from train_station tr_st 
                                        INNER JOIN station st ON (tr_st.station_id = st.id) 
                                        INNER JOIN train tr ON (tr.id = tr_st.train_id)
                                        WHERE tr_st.train_id = %s
                                        ORDER BY tr_st."order";""", (train[1],))
                out[train[0]] = [self.cursor.fetchall()]
                self.cursor.execute(f"""SELECT tv.order, stv.remained_sits, tvt.price, vt.name FROM scheduled_train_vagon stv
                                        INNER JOIN train_vagon tv ON (tv.id = stv.train_vagon_id)
                                        INNER JOIN vagon v ON (v.id = tv.vagon_id)
                                        INNER JOIN vagon_type vt ON (vt.id = v.vagon_type_id)
                                        INNER JOIN train_vagon_type tvt ON(tvt.train_id = tv.train_id AND vt.id = tvt.vagon_type_id)
                                        WHERE tv.train_id = %s AND scheduled_train_id = %s
                                        ORDER BY tv.order;""", (train[1], train[0]))
                out[train[0]].append(self.cursor.fetchall())
                out[train[0]].append(train[2])
            return {'code': 0, 'data': out}
        elif req['type'] == RequestType.ORDER_TRAIN:
            if self.user_id is None:
                return {'code': 401, 'msg': 'Неавторизованный пользователь не может заказывать билеты'}

            self.cursor.execute(f'''SELECT order__train(%s, %s, %s, %s, %s);''',
                                (req['data']['start_st'], req['data']['end_st'], req['data']['scheduled_train_id'],
                                 req['data']['vagon_num'], self.user_id))
            return {'code': self.cursor.fetchone()[0]}
        elif req['type'] == RequestType.LIST_ORDERS:
            if self.user_id is None:
                return {'code': 401, 'msg': 'У неавторизованного пользователя нет билетов'}
            self.cursor.execute(f'''SELECT DISTINCT str.start_dt::VARCHAR, o.sit_num, train_vagon."order", st.name, st2.name, o.ordered_dt::VARCHAR, o.paid
                                FROM "order" o
                                INNER JOIN station st ON (st.id = o.start_station_id)
                                INNER JOIN station st2 ON (st2.id = o.end_station_id)
                                INNER JOIN scheduled_train  str ON (o.scheduled_train_id = str.id)
                                INNER JOIN train_vagon ON (train_vagon.id = o.train_vagon_id)
                                WHERE user_id = %s;''', (self.user_id,))

            return {'code': 0, 'data': self.cursor.fetchall()}
        else:
            return {'code': 405, 'msg': 'unsupported command'}


def handle_user(user_conn, addr):
    try:
        session = Session(conn.cursor())
        while True:
            msg = Protocol.read_message(user_conn)
            if msg is None:
                print(f"{addr} disconnected")
                break
            print(f"{addr}: {msg}")
            response_msg = session.handle_request(msg)
            Protocol.send_message(user_conn, response_msg)

    except psycopg2.DatabaseError as e:
        print(f'Error {e} with addr {addr}')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--reset_db', action='store_true', default=False)
    args = parser.parse_args()

    conn = None
    s = None
    try:
        conn = psycopg2.connect(
            host=os.getenv('POSTGRES_HOST'),
            database=os.getenv('POSTGRES_DB'),
            user=os.getenv('POSTGRES_USER'),
            password=os.getenv('POSTGRES_PASSWORD'),
            port=os.getenv('POSTGRES_PORT')
        )
        conn.autocommit = True

        if args.reset_db:
            execute_file('set_up_db.sql')
            set_up_scheduled_trains(range(21, -1, -1))

        _thread.start_new_thread(continuous_train_addition, tuple())
        _thread.start_new_thread(continuous_train_deletion, tuple())

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind((HOST, PORT))
            s.listen()
            print('Started listening...')
            while True:
                user_conn, addr = s.accept()
                print(f"Connected {addr}")
                _thread.start_new_thread(handle_user, (user_conn, addr))

    except psycopg2.DatabaseError as e:
        print(f'Error {e}')

    finally:
        if s:
            s.close()
        if conn:
            conn.close()
