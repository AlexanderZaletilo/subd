import os
import getpass
import socket

from prettytable import PrettyTable

from shared import Protocol, RequestType


HOST = 'localhost'
PORT = int(os.getenv('SERVER_PORT'))


class Actions:

    @staticmethod
    def authorize():
        global current_user
        while True:
            username = input("Имя пользователя: ")
            password = getpass.getpass("Пароль: ")
            Protocol.send_message(conn, {'type': RequestType.AUTHORIZE,
                                         'data': {
                                             'username': username,
                                             'password': password
                                         }})
            response = Protocol.read_message(conn)
            if response['code'] == 0:
                print('Успешная авторизация!')
                current_user = username
                break
            elif response['code'] in (1, 2):
                print("Неверный логин и/или пароль")
            else:
                print("Неизвестная ошибка")

    @staticmethod
    def register():
        while True:
            username = input("Имя пользователя: ")
            password = getpass.getpass("Пароль: ")
            Protocol.send_message(conn, {'type': RequestType.REGISTER,
                                         'data': {
                                             'username': username,
                                             'password': password
                                         }})
            response = Protocol.read_message(conn)
            if response['code'] == 0:
                print('Успешная регистрация!')
                break
            elif response['code'] == 1:
                print("Пользователь с таким именем уже существует")
            elif response['code'] == 2:
                print("Пароль должен быть минимум 8 символов")
            else:
                print("Неизвестная ошибка")

    @staticmethod
    def logout():
        global current_user
        if current_user is None:
            print("Вы и так не вошли")
        else:
            Protocol.send_message(conn, {'type': RequestType.LOGOUT})
            response = Protocol.read_message(conn)
            if response['code'] == 0:
                print('Успешный выход!')
                current_user = None
            else:
                print("Неизвестная ошибка")

    @staticmethod
    def find_trains():
        from_st = to_st = where = ''
        while not from_st or not to_st or not where:
            from_st = input("Откуда: ")
            to_st = input("Куда: ")
            where = input("Когда(YYYY-MM-DD): ")

        Protocol.send_message(conn, {'type': RequestType.FIND_TRAIN,
                                     'data': {'from_st': from_st, 'to_st': to_st, 'where': where}})
        response = Protocol.read_message(conn)
        if response['code'] == 0:
            x = PrettyTable()
            x.field_names = ['Код', 'Время','Название', 'Станции', 'Длительность(мин)', 'Вагоны', 'Мест', 'Цена']
            x.align["Станции"] = x.align["Длительность(мин)"] = x.align["Вагоны"] = "l"
            for train, train_data in response['data'].items():
                stations, vagons, dt = train_data
                name = f"{stations[0][0]}. {stations[0][1]} - {stations[-1][1]}"
                stations_str = '\n'.join(f"{st[2]}. {st[1]}" for st in stations)
                stations_length_str = '\n'.join(f"{st[3]} мин." for st in stations)
                vagons1_str = "\n".join(f"{v[0]}. {v[3]}" for v in vagons)
                vagons2_str = "\n".join(f"{v[1]}" for v in vagons)
                mul1 =  mul2 = 0
                for st in stations:
                    if st[1] == from_st:
                        mul1 = st[-1]
                    if st[1] == to_st:
                        mul2 = st[-1]
                mul = mul2 - mul1
                vagons3_str = "\n".join(f"{int((v[-2] * mul) / 100)}.{int((v[-2] * mul)) % 100:<02}" for v in vagons)
                x.add_row([train, dt.split(' ')[-1], name, stations_str, stations_length_str,
                           vagons1_str, vagons2_str, vagons3_str])
            print(x)
        else:
            print("Неизвестная ошибка")

    @staticmethod
    def order_train():
        from_st = input("Откуда: ")
        to_st = input("Куда: ")
        code = int(input("Код поезда: "))
        vagon_num = int(input("Номер вагона: "))

        Protocol.send_message(conn, {'type': RequestType.ORDER_TRAIN,
                                     'data': {'start_st': from_st, 'end_st': to_st, 'scheduled_train_id': code, 'vagon_num': vagon_num}})
        response = Protocol.read_message(conn)
        if response['code'] == 0:
            print('Билет куплен!')
        elif response['code'] == 1:
            print('Данный поезд не существует/уже прошёл')
        elif response['code'] == 2:
            print('Данного вагона нет/нет мест')
        elif response['code'] == 3:
            print('Данный поезд данные станции не проезжает')
        elif response['code'] == 401:
            print('Неавторизован')
        else:
            print("Неизвестная ошибка")

    @staticmethod
    def list_orders():
        Protocol.send_message(conn, {'type': RequestType.LIST_ORDERS})
        response = Protocol.read_message(conn)
        if response['code'] == 0:
            x = PrettyTable()
            x.field_names = ['Дата выезда', 'Место', 'Вагон', 'Начало', 'Конец', 'Дата покупки', 'Заплачено']
            x.add_rows(response['data'])
            print(x)
        elif response['code'] == 401:
            print('Неавторизован')
        else:
            print("Неизвестная ошибка")
    @classmethod
    def list_actions(cls):
        for key, value in cls.mapping.items():
            print(f"{key:>2}. {value[1]}")


Actions.mapping = {
    1: (Actions.authorize, "Авторизация"),
    2: (Actions.register, 'Регистрация'),
    3: (Actions.list_actions, 'Список команд'),
    4: (Actions.logout, 'Выйти'),
    5: (Actions.find_trains, 'Найти поезда'),
    6: (Actions.order_train, 'Купить билет на поезд'),
    7: (Actions.list_orders, 'Список купленных билетов')
}


if __name__ == '__main__':
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as conn:
        conn.connect((HOST, PORT))
        Actions.list_actions()
        current_user = None
        while True:
            try:
                command = int(input(f"Следующая команда{'(Текущий пользователь ' + current_user + ')' if current_user is not None else ''}: "))
            except Exception as ex:
                print(f"неверная команда")
                continue
            Actions.mapping[command][0]()


