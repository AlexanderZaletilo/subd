import json
from enum import IntEnum, auto


class RequestType(IntEnum):
    AUTHORIZE = auto()
    REGISTER = auto()
    LOGOUT = auto()
    FIND_TRAIN = auto()
    ORDER_TRAIN = auto()
    LIST_ORDERS = auto()


class Protocol:

    @staticmethod
    def read_message_length(user_conn):
        data = user_conn.recv(4)
        if not data:
            return 0
        return int.from_bytes(data, 'big')

    @staticmethod
    def read_message(user_conn):
        msg_len = Protocol.read_message_length(user_conn)
        if not msg_len:
            return None
        return json.loads(user_conn.recv(msg_len))

    @staticmethod
    def calc_message_length(msg):
        return int.to_bytes(len(msg), 4, 'big')

    @staticmethod
    def send_message(user_conn, json_msg):
        msg = json.dumps(json_msg, ensure_ascii=False).encode()
        msg_len = Protocol.calc_message_length(msg)
        user_conn.sendall(msg_len + msg)
