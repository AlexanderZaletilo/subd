-- drop all the existing data
DROP TABLE IF EXISTS "user" CASCADE;
DROP TABLE IF EXISTS train CASCADE;
DROP TABLE IF EXISTS station CASCADE;
DROP TABLE IF EXISTS train_station CASCADE;
DROP TABLE IF EXISTS vagon_type CASCADE;
DROP TABLE IF EXISTS vagon CASCADE;
DROP TABLE IF EXISTS train_vagon CASCADE;
DROP TABLE IF EXISTS train_vagon_type CASCADE;
DROP TABLE IF EXISTS scheduled_train_vagon CASCADE;
DROP TABLE IF EXISTS scheduled_train CASCADE;
DROP TABLE IF EXISTS "order" CASCADE;


-- create tables
CREATE TABLE IF NOT EXISTS "user"(
    id SERIAL PRIMARY KEY,
    username VARCHAR(32) NOT NULL,
    password CHAR(32) NOT NULL,
    joined TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS vagon_type(
    id SERIAL2 PRIMARY KEY,
    name VARCHAR(255) NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS vagon(
    id serial PRIMARY KEY,
    capacity SMALLINT NOT NULL CHECK (capacity > 0),
    vagon_type_id SMALLINT NOT NULL,
    FOREIGN KEY (vagon_type_id) REFERENCES vagon_type (id)
);

CREATE TABLE IF NOT EXISTS station(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS train(
    id SERIAL2 PRIMARY KEY,
    code CHAR(4) UNIQUE,
    cron_schedule VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS train_station(
    train_id SMALLINT NOT NULL,
    station_id INTEGER NOT NULL,
    "order" SMALLINT NOT NULL CHECK("order" > 0),
    elapsed_minutes SMALLINT NOT NULL CHECK (elapsed_minutes >= 0),
    multiplier FLOAT NOT NULL CHECK (multiplier >= 0.0 AND multiplier <= 1.0),
    PRIMARY KEY (train_id, station_id),
    FOREiGN KEY (train_id) REFERENCES train(id),
    FOREIGN KEY (station_id) REFERENCES station(id)
);

CREATE INDEX train_order_index ON train_station
( train_id, "order" DESC);

CREATE TABLE IF NOT EXISTS train_vagon(
    id SERIAL PRIMARY KEY,
    train_id SMALLINT NOT NULL,
    vagon_id INTEGER NOT NULL,
    "order" SMALLINT NOT NULL CHECK("order" > 0),
    FOREiGN KEY (train_id) REFERENCES train(id),
    FOREIGN KEY (vagon_id) REFERENCES vagon(id)
);

CREATE INDEX train_vagon_index ON train_vagon
( train_id, vagon_id);

CREATE TABLE IF NOT EXISTS train_vagon_type(
    train_id SMALLINT NOT NULL,
    vagon_type_id SMALLINT NOT NULL,
    price SMALLINT NOT NULL,
    PRIMARY KEY(train_id, vagon_type_id),
    FOREiGN KEY (train_id) REFERENCES train(id),
    FOREIGN KEY (vagon_type_id) REFERENCES vagon_type(id)
);

CREATE TABLE IF NOT EXISTS scheduled_train(
    id SERIAL PRIMARY KEY,
    train_id SMALLINT NOT NULL,
    start_dt TIMESTAMP WITH TIME ZONE NOT NULL,
    done BOOLEAN NOT NULL DEFAULT FALSE,
    FOREIGN KEY (train_id) REFERENCES train(id),
    UNIQUE (train_id, start_dt)
);

CREATE INDEX scheduled_train_index ON scheduled_train
(done, start_dt);

CREATE TABLE IF NOT EXISTS scheduled_train_vagon(
    scheduled_train_id INT NOT NULL,
    train_vagon_id INT NOT NULL,
    remained_sits SMALLINT NOT NULL CHECK (remained_sits >= 0),
    PRIMARY KEY(scheduled_train_id, train_vagon_id),
    FOREIGN KEY (train_vagon_id) REFERENCES train_vagon(id)
);

CREATE TABLE IF NOT EXISTS "order"(
    id SERIAL8 PRIMARY KEY,
    user_id SERIAL NOT NULL,
    scheduled_train_id INTEGER NOT NULL,
    sit_num SMALLINT NOT NULL,
    train_vagon_id INTEGER NOT NULL,
    start_station_id INTEGER NOT NULL,
    end_station_id INTEGER NOT NULL,
    ordered_dt TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL,
    paid SMALLINT NOT NULL,
    FOREIGN KEY (scheduled_train_id) REFERENCES scheduled_train(id),
    FOREIGN KEY (user_id) REFERENCES "user"(id),
    FOREIGN KEY (start_station_id) REFERENCES  station(id),
    FOREIGN KEY (end_station_id) REFERENCES  station(id),
    UNIQUE(scheduled_train_id, train_vagon_id)
);


-- create functions
CREATE OR REPLACE FUNCTION register_user(name varchar(32), password varchar(32))
        RETURNS INT
        LANGUAGE plpgsql
    AS
$$
BEGIN
    IF EXISTS(SELECT 1 FROM "user" WHERE "user".username = name) THEN
        RETURN 1; -- this user already exists
    ELSIF char_length(password) < 8 THEN
        RETURN 2; -- short password
    ELSE
        INSERT INTO "user"(username, password) VALUES
        (name, MD5(password));
        RETURN 0;
    END IF;
END;
$$;

CREATE OR REPLACE FUNCTION authorize(name varchar(32), password varchar(32))
    RETURNS INT
    LANGUAGE plpgsql
    AS
$$
BEGIN
    IF NOT EXISTS(SELECT 1 FROM "user" WHERE "user".username = name) THEN
        RETURN 1; -- user doesn't exists
    ELSIF (SELECT "user".password FROM "user" WHERE "user"."username" = name) != MD5(password) THEN
        RETURN 2; -- wrong password
    ELSE
        RETURN 0;
    END IF;
END;
$$;

CREATE OR REPLACE PROCEDURE schedule_train(train_id INT, start_dt TIMESTAMP WITH TIME ZONE)
  LANGUAGE plpgsql
    AS
$$
DECLARE
    st_id INT;
BEGIN
    INSERT INTO scheduled_train(train_id, start_dt) VALUES (train_id, start_dt) ON CONFLICT DO NOTHING RETURNING id INTO st_id;
    IF st_id IS NOT NULL THEN
        INSERT INTO scheduled_train_vagon(scheduled_train_id, train_vagon_id, remained_sits)
        SELECT st_id, train_vagon.id, vagon.capacity
        FROM train_vagon INNER JOIN vagon ON (vagon.id = train_vagon.vagon_id);
    END IF;

END;
$$;

CREATE OR REPLACE FUNCTION order__train(start_st VARCHAR(255), end_st VARCHAR(255), _scheduled_train_id INT, vagon_num INT , user_id INT)
  RETURNS INT
  LANGUAGE plpgsql
    AS
$$
DECLARE
    tv_id INT;
    sit_num INT;
    multiplier FLOAT;
    start_st_id INT;
    end_st_id INT;
    vagon_id INT;
    t_id INT;
    price INT;
BEGIN
    IF NOT EXISTS(SELECT 1 FROM scheduled_train WHERE done = FALSE and start_dt > current_timestamp) THEN
        RETURN 1;
    ELSE
        SELECT tv.id, tv.train_id, tv.vagon_id FROM scheduled_train_vagon stv
           INNER JOIN train_vagon tv ON (tv.id = stv.train_vagon_id)
           WHERE stv.scheduled_train_id = _scheduled_train_id AND tv."order" = vagon_num and stv.remained_sits != 0 INTO tv_id, t_id, vagon_id;
        IF tv_id IS NULL THEN
            RETURN 2;
        ELSIF NOT EXISTS (SELECT sc_tr.train_id FROM scheduled_train sc_tr
                            INNER JOIN train_station tr_st ON (sc_tr.train_id = tr_st.train_id)
                            INNER JOIN station st ON (tr_st.station_id = st.id)
                            WHERE sc_tr.id = _scheduled_train_id
                            GROUP BY (sc_tr.id, sc_tr.train_id)
                            HAVING string_agg(st.name, '|') LIKE '%'||start_st||'%'||end_st||'%') THEN
            RETURN 3;
        ELSE
            UPDATE scheduled_train_vagon stv SET remained_sits = remained_sits - 1
            WHERE stv.scheduled_train_id = _scheduled_train_id AND stv.train_vagon_id = tv_id RETURNING remained_sits + 1 INTO sit_num;

            SELECT id from station WHERE name = start_st INTO start_st_id;
            SELECT id from station WHERE name = end_st INTO end_st_id;

            multiplier := ((SELECT ts.multiplier FROM train_station ts WHERE train_id = t_id AND station_id = end_st_id)
                            - (SELECT ts.multiplier FROM train_station ts WHERE train_id = t_id AND station_id = start_st_id));
            SELECT tvt.price * multiplier from train_vagon_type tvt
                                        INNER JOIN vagon v ON(tvt.vagon_type_id = v.vagon_type_id)
                                        WHERE v.id = vagon_id AND train_id = t_id INTO price;
            INSERT INTO "order"(user_id, scheduled_train_id, sit_num, train_vagon_id,
                                start_station_id, end_station_id, paid) VALUES
             (user_id, _scheduled_train_id, sit_num, tv_id,
              start_st_id, end_st_id, price);

            RETURN 0;
        END IF;
    END IF;

END;
$$;
-- insert test data

SELECT register_user('user', 'password');

INSERT INTO station(name) VALUES
('Минск-Пассажирский'), ('Молодечно'), ('Полоцк'), ('Витебск'),
('Оболь'), ('Шумилино'), ('Гомель'), ('Брест'), ('Гродно'), ('Могилёв'),
('Орша'), ('Слуцк'), ('Лида'), ('Пинск'), ('Вилейка'), ('Парафьянов'),
('Крулевщизна'), ('Подсвилье');

INSERT INTO vagon_type(name) VALUES
('Плацкартный'), ('Купейный'), ('Сидячий');

INSERT INTO vagon(capacity, vagon_type_id) VALUES
(52, 1), (40, 2), (150, 3);

INSERT INTO train(code, cron_schedule) VALUES
('704Б', '30 16 * * *'), ('871Б', '30 6 * * 1-5');

INSERT INTO train_vagon(train_id, vagon_id, "order") VALUES
(1, 1, 1), (1, 2, 2), (1, 1, 3), -- first train with 3 vagons
(2, 3, 1), (2, 3, 2); -- second train with 2 vagons

INSERT INTO train_vagon_type(train_id, vagon_type_id, price) VALUES
(1, 1, 1000), (1, 2, 750), (2, 1, 500), (2, 3, 200);

INSERT INTO train_station(train_id, station_id, "order", elapsed_minutes, multiplier) VALUES
-- first train with
(1, 1, 1, 0, 0.0), -- Минск Пассажирский
(1, 3, 2, 141, 0.4), -- Полоцк
(1, 4, 3, 241, 1.0), -- Витебск(16.04 BYN)
-- second train

(2, 1, 1, 0, 0.0), -- Минск Пассажирский
(2, 2, 2, 50, 0.4), -- Молодечно
(2, 15, 3, 90, 0.7), -- Вилейка
(2, 3, 4, 150, 1.0); -- Полоцк